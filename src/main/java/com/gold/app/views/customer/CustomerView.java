package com.gold.app.views.customer;

import com.gold.app.Transaction;
import com.gold.app.views.MainLayout;
import com.vaadin.flow.component.Text;
import com.vaadin.flow.component.accordion.Accordion;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.grid.Grid;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.router.PageTitle;
import com.vaadin.flow.router.Route;

@PageTitle("Customer")
@Route(value = "customer", layout = MainLayout.class)
public class CustomerView extends VerticalLayout {

    public CustomerView() {
        Accordion accordionTopUp = new Accordion();
        accordionTopUp.setWidthFull();

        TextField textFieldTopUpGram = new TextField("Jumlah gram");
        Button buttonTopUp = new Button("Top up");

        VerticalLayout layoutTopUp = new VerticalLayout(
                textFieldTopUpGram,
                buttonTopUp
        );
        layoutTopUp.setSpacing(false);
        layoutTopUp.setPadding(false);

        accordionTopUp.add(
                "Top Up",
                layoutTopUp
        );

        add(accordionTopUp);


        Accordion accordionBalance = new Accordion();
        accordionBalance.setWidthFull();

        Text textBalance = new Text("Saldo: ");

        VerticalLayout layoutBalance = new VerticalLayout(
                textBalance
        );
        layoutBalance.setSpacing(false);
        layoutBalance.setPadding(false);

        accordionBalance.add(
                "Saldo",
                layoutBalance
        );

        add(accordionBalance);


        Accordion accordionMutation = new Accordion();
        accordionMutation.setWidthFull();

        Grid<Transaction> gridMutation = new Grid<>(Transaction.class, false);
        gridMutation.addColumn(Transaction::getDate).setHeader("Tanggal");
        gridMutation.addColumn(Transaction::getType).setHeader("Jenis Transaksi");
        gridMutation.addColumn(Transaction::getGram).setHeader("Jumlah Gram");
        gridMutation.addColumn(Transaction::getTopUpPrice).setHeader("Harga Top Up");
        gridMutation.addColumn(Transaction::getBuybackPrice).setHeader("Harga Buyback");

        VerticalLayout layoutMutation = new VerticalLayout(
                gridMutation
        );
        layoutMutation.setSpacing(false);
        layoutMutation.setPadding(false);

        accordionMutation.add(
                "Mutasi",
                layoutMutation
        );

        add(accordionMutation);


        Accordion accordionBuyback = new Accordion();
        accordionBuyback.setWidthFull();

        TextField textFieldBuybackGram = new TextField("Jumlah gram");
        Button buttonBuyback = new Button("Jual");

        VerticalLayout layoutBuyback = new VerticalLayout(
                textFieldBuybackGram,
                buttonBuyback
        );
        layoutBuyback.setSpacing(false);
        layoutBuyback.setPadding(false);

        accordionBuyback.add(
                "Penjualan",
                layoutBuyback
        );

        add(accordionBuyback);
    }

}
