package com.gold.app.views.admin;

import com.gold.app.views.MainLayout;
import com.vaadin.flow.component.accordion.Accordion;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.router.PageTitle;
import com.vaadin.flow.router.Route;
import com.vaadin.flow.router.RouteAlias;

@PageTitle("Admin")
@Route(value = "admin", layout = MainLayout.class)
@RouteAlias(value = "", layout = MainLayout.class)
public class AdminView extends VerticalLayout {

    public AdminView() {
        Accordion accordionPriceSetting = new Accordion();
        accordionPriceSetting.setWidthFull();

        TextField textFieldPrice = new TextField("Harga");
        Button buttonSave = new Button("Simpan");

        VerticalLayout layoutPriceSetting = new VerticalLayout(
                textFieldPrice,
                buttonSave
        );
        layoutPriceSetting.setSpacing(false);
        layoutPriceSetting.setPadding(false);

        accordionPriceSetting.add(
                "Pengaturan Harga",
                layoutPriceSetting
        );

        add(accordionPriceSetting);
    }

}
