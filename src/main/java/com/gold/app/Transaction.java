package com.gold.app;

import java.sql.Date;

public class Transaction {
    private Date date;
    private String type;
    private float gram;
    private int topUpPrice;
    private int buybackPrice;
    private float balance;

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public float getGram() {
        return gram;
    }

    public void setGram(float gram) {
        this.gram = gram;
    }

    public int getTopUpPrice() {
        return topUpPrice;
    }

    public void setTopUpPrice(int topUpPrice) {
        this.topUpPrice = topUpPrice;
    }

    public int getBuybackPrice() {
        return buybackPrice;
    }

    public void setBuybackPrice(int buybackPrice) {
        this.buybackPrice = buybackPrice;
    }

    public float getBalance() {
        return balance;
    }

    public void setBalance(float balance) {
        this.balance = balance;
    }
}
